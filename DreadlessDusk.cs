using System;

namespace DreadlessDuskTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            char usrcha = ' '; string strusrcha = Convert.ToString(usrcha);
            int level = 1; int damage = 2; int health = 10; int speed = 3; int point = 1;
            Console.Write("~~d r e a d l e s s    d u s k~~\n\n\n" +
                              "enter your character's name...\t");
            string usrname = Console.ReadLine(); Console.Clear();
            Console.WriteLine("{0}... {0} is all you know of.\nAll you remember " +
                              "is {0}...\n", usrname); Console.ReadLine();
            Random rnd = new Random();
            int ranint = rnd.Next(1, 5); int inturn = 1;
            string environment = ""; string weather = ""; string sound = "";
            if (ranint == 1) { environment = "swamp"; }
            if (ranint == 2) { environment = "forest"; }
            if (ranint == 3) { environment = "castle"; }
            if (ranint == 4) { environment = "desert"; }
            ranint = rnd.Next(1, 5);
            if (ranint == 1) { weather = "stormy and rainy"; }
            if (ranint == 2) { weather = "calm but cool"; }
            if (ranint == 3) { weather = "freezing but still"; }
            if (ranint == 4) { weather = "humid and hot"; }
            ranint = rnd.Next(1, 5);
            if (ranint == 1) { sound = "Growls"; }
            if (ranint == 2) { sound = "Shrieks"; }
            if (ranint == 3) { sound = "Groans"; }
            if (ranint == 4) { sound = "Rustlings"; }
            Console.WriteLine("Looking around you find yourself in a {0}.\n" +
                              "The weather is {1}, and the skys are nearly black.\n" +
                              "{2} echo from your surroundings.\nJumping into action" +
                              " you realise there is no time to waste.\n" +
                              "Survive now so you can think later..."
                              , environment, weather, sound);
            Console.ReadLine(); Console.Clear();
            bool game = true; while (game == true)
            {
                while (point != 0)
                {
                    Console.WriteLine("{0}\t\tdamage:{1} health:{2} speed:{3}\n"
                                      , usrname, damage, health, speed);
                    Console.WriteLine("you have reached level {0}, and have gained a skill point\n\n" +
                                      "press 1, 2, or 3 to spend a skill point on what you want:\n" +
                                      "1)\tdamage: {1} (+1)\n" +
                                      "2)\thealth: {2} (+2)\n" +
                                      "3)\tspeed:  {3} (+2)\n", level, damage, health, speed);
                    usrcha = Console.ReadKey().KeyChar;
                    if (usrcha == '1') { damage = damage + 1; point = point - 1; }
                    else if (usrcha == '2') { health = health + 2; point = point - 1; }
                    else if (usrcha == '3') { speed = speed + 2; point = point - 1; }
                    else { Console.Clear();Console.WriteLine("invalid input."); }
                }
                bool turn = true; while (turn == true)
                {
                    Console.Clear();
                    Console.WriteLine("Lv:{5} {0}\t\tdamage:{1} health:{2} speed:{3}\tturn:{4}\n"
                                      , usrname, damage, health, speed, inturn, level);
                    ranint = rnd.Next(1, 11);
                    if (ranint == 1) { }
                    if (ranint == 2) { }
                    if (ranint == 3) { }
                    if (ranint == 1) { }
                    if (ranint == 5) { }
                    if (ranint == 6) { }
                    if (ranint == 7) { }
                    if (ranint == 8) { }
                    if (ranint == 9) { }
                    if (ranint == 10) { }
                    Console.ReadLine(); inturn = inturn + 1; turn = false;
                    ranint = rnd.Next(1, 4);
                    if (ranint == 3) { level = level + 1; point = point + 1; }
                    Console.Clear();
                }
                if (health == 0)
                {
                    game = false;
                }
            }
            Console.WriteLine("\n\n~~g a m e    o v e r~~");
        }
    }
}
